from django.db import models
from DjangoParaugs.izdevnieciba.models import izdevnieciba
from DjangoParaugs.autors.models import autors

class gramata(models.Model):

    nosaukums = models.CharField(max_length=45)
    izdosanas_gads = models.IntegerField()
    izdevnieciba_id = models.ForeignKey(izdevnieciba, db_column='izdevnieciba_id', on_delete=models.DO_NOTHING)
    autori = models.ManyToManyField(autors, through='gramatas_autori')

    class Meta:
        db_table = 'gramata'


class gramatas_autori(models.Model):
    
    gramata_id = models.ForeignKey(gramata, db_column='gramata_id', on_delete=models.CASCADE)
    autors_id = models.ForeignKey(autors, db_column='autors_id', on_delete=models.CASCADE)
    pievienosanas_datums = models.DateTimeField()

    class Meta:
        db_table = 'gramata_autors'