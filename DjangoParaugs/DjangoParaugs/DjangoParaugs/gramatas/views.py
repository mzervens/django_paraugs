from django.http import HttpResponse
from django.template import loader
from DjangoParaugs.gramatas.models import gramata, gramatas_autori
from DjangoParaugs.izdevnieciba.models import izdevnieciba
from DjangoParaugs.autors.models import autors
import datetime

# list
def index(request):
    allGramatas = gramata.objects.all()

    template = loader.get_template("gramata/gramata.html")
    context = {'gramatas': allGramatas}

    return HttpResponse(template.render(context, request))

# detailed view
def detailed_view(request, id):

    gramataData = gramata.objects.get(pk=id)
    template = loader.get_template("gramata/detailed.html")
    context = {'gramata': gramataData}

    return HttpResponse(template.render(context, request))


def validate_create_input(input):
    valid = True

    if not('publisher' in input and 'authors' in input and
           'title' in input and 'publish_date' in input):
        valid = False

    elif input['title'] == "":
        valid = False

    elif len(input['publisher']) <= 0 or len(input['authors']) <= 0:
        valid = False

    try:
        int(input['publish_date'])
    except ValueError:
        valid = False

    return valid

# new entry creation
def create(request):

    template = loader.get_template("gramata/create.html")
    context = {}

    if request.method == "GET":
        izd_all = izdevnieciba.objects.all()
        aut_all = autors.objects.all()
        context['publishers'] = izd_all
        context['authors'] = aut_all

        return HttpResponse(template.render(context, request))
    else:
        context['type'] = "POST"
        context['success'] = validate_create_input(request.POST)

        '''
        context['input'] = str(request.POST)
        '''

        if context['success'] :
            newBook = gramata(nosaukums=request.POST['title'], izdosanas_gads=request.POST['publish_date'])
            
            publisher = izdevnieciba.objects.get(pk=request.POST['publisher'])
            newBook.izdevnieciba_id = publisher

            # need to save so that there is generated new book id which is used in the manytomany relationship
            newBook.save()

            for i, authorId in enumerate(request.POST.getlist('authors')):
                bookAuth = autors.objects.get(pk=authorId)
                customMany2Many = gramatas_autori(gramata_id=newBook, autors_id=bookAuth, pievienosanas_datums=datetime.datetime.now())
                customMany2Many.save()

        return HttpResponse(template.render(context, request))


def delete(request, id):

    gram = gramata.objects.get(pk=id)
    gram.delete()

    return index(request)
