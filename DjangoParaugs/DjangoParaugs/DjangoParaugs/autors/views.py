from django.http import HttpResponse
from django.template import loader
from DjangoParaugs.autors.models import autors

def index(request):
    allAutori = autors.objects.all()

    template = loader.get_template("autors/autors.html")
    context = {'autori': allAutori}

    return HttpResponse(template.render(context, request))
