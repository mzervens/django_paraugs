from django.db import models

class autors(models.Model):
    vards = models.CharField(max_length=45)
    uzvards = models.CharField(max_length=45)
    vecums = models.IntegerField()

    class Meta:
        db_table = 'autors'