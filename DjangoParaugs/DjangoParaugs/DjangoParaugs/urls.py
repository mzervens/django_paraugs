"""
Definition of urls for DjangoParaugs.
"""

from django.conf.urls import include, url
from . import views

from DjangoParaugs.autors.views import index as autorsList
from DjangoParaugs.gramatas.views import index as gramataList
from DjangoParaugs.gramatas.views import detailed_view
from DjangoParaugs.gramatas.views import create as gramataCreate
from DjangoParaugs.gramatas.views import delete as gramataDelete
from DjangoParaugs.izdevnieciba.views import index as izdevniecibaList

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = [
    
    url(r'^$', views.index),
    url(r'^/$', views.index),
    url(r'^index/$', views.index),

    url(r'^autors$', autorsList),
    
    url(r'^gramata/$', gramataList),
    url(r'^gramata/(\d+)$', detailed_view),
    url(r'^gramata/create', gramataCreate),
    url(r'^gramata/delete/(\d+)$', gramataDelete),
    

    url(r'^izdevnieciba/$', izdevniecibaList),

    # Examples:
    # url(r'^$', 'DjangoParaugs.views.home', name='home'),
    # url(r'^DjangoParaugs/', include('DjangoParaugs.DjangoParaugs.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
]
