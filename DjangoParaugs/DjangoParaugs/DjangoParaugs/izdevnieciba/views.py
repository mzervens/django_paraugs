from django.http import HttpResponse
from django.template import loader
from DjangoParaugs.izdevnieciba.models import izdevnieciba

def index(request):
    allIzdevniecibas = izdevnieciba.objects.all()

    template = loader.get_template("izdevnieciba/izdevnieciba.html")
    context = {'izdevniecibas': allIzdevniecibas}

    return HttpResponse(template.render(context, request))