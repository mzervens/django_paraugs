�}q (X   docqX�  This module provides mechanisms to use signal handlers in Python.

Functions:

alarm() -- cause SIGALRM after a specified time [Unix only]
setitimer() -- cause a signal (described below) after a specified
               float time and the timer may restart then [Unix only]
getitimer() -- get current value of timer [Unix only]
signal() -- set the action for a given signal
getsignal() -- get the signal action for a given signal
pause() -- wait until a signal arrives [Unix only]
default_int_handler() -- default SIGINT handler

signal constants:
SIG_DFL -- used to refer to the system default handler
SIG_IGN -- used to ignore the signal
NSIG -- number of defined signals
SIGINT, SIGTERM, etc. -- signal numbers

itimer constants:
ITIMER_REAL -- decrements in real time, and delivers SIGALRM upon
               expiration
ITIMER_VIRTUAL -- decrements only when the process is executing,
               and delivers SIGVTALRM upon expiration
ITIMER_PROF -- decrements both when the process is executing and
               when the system is executing on behalf of the process.
               Coupled with ITIMER_VIRTUAL, this timer is usually
               used to profile the time spent by the application
               in user and kernel space. SIGPROF is delivered upon
               expiration.


*** IMPORTANT NOTICE ***
A signal handler function is called with two arguments:
the first is the signal number, the second is the interrupted stack frame.qX   membersq}q(X   __name__q}q(X   valueq}qX   typeq	]q
X   builtinsqX   strq�qasX   kindqX   dataquX   __doc__q}q(h}qh	]qhashhuX	   getsignalq}q(h}q(hX$  Return the current action for the given signal.

The return value can be:
  SIG_IGN -- if the signal is being ignored
  SIG_DFL -- if the default action for the signal is in effect
  None    -- if an unknown handler is in effect
  anything else -- the callable Python object used as a handlerqX	   overloadsq]q}q(X   argsq}q(X
   arg_formatqX   *qX   nameqhu}q (hX   **q!hX   kwargsq"u�q#hX$  Return the current action for the given signal.

The return value can be:
  SIG_IGN -- if the signal is being ignored
  SIG_DFL -- if the default action for the signal is in effect
  None    -- if an unknown handler is in effect
  anything else -- the callable Python object used as a handlerq$uauhX   functionq%uX
   __loader__q&}q'(h]q(X   _frozen_importlibq)X   BuiltinImporterq*�q+ahX   typerefq,uX   set_wakeup_fdq-}q.(h}q/(hX�   set_wakeup_fd(fd) -> fd

Sets the fd to be written to (with the signal number) when a signal
comes in.  A library can use this to wakeup select or poll.
The previous fd or -1 is returned.

The fd must be non-blocking.q0h]q1}q2(h}q3hX   fdq4s�q5hX�   Sets the fd to be written to (with the signal number) when a signal
comes in.  A library can use this to wakeup select or poll.
The previous fd or -1 is returned.

The fd must be non-blocking.q6X   ret_typeq7]q8hX   intq9�q:auauhh%uX   signalq;}q<(h}q=(hXQ  Set the action for the given signal.

The action can be SIG_DFL, SIG_IGN, or a callable Python object.
The previous action is returned.  See getsignal() for possible return values.

*** IMPORTANT NOTICE ***
A signal handler function is called with two arguments:
the first is the signal number, the second is the interrupted stack frame.q>h]q?}q@(h}qA(hhhhu}qB(hh!hh"u�qChXQ  Set the action for the given signal.

The action can be SIG_DFL, SIG_IGN, or a callable Python object.
The previous action is returned.  See getsignal() for possible return values.

*** IMPORTANT NOTICE ***
A signal handler function is called with two arguments:
the first is the signal number, the second is the interrupted stack frame.qDuauhh%uX   SIGFPEqE}qF(h}qGh	]qHhX   intqI�qJashhuX   SIGINTqK}qL(h}qMh	]qNhJashhuX   SIGTERMqO}qP(h}qQh	]qRhJashhuh*}qS(h}qT(X   basesqU]qVhX   objectqW�qXah}qY(X   __setattr__qZ}q[(h}q\(hX%   Implement setattr(self, name, value).q]h]q^}q_(h}q`(hhhhu}qa(hh!hh"u�qbhX%   Implement setattr(self, name, value).qcuauhX   methodqduX   __delattr__qe}qf(h}qg(hX   Implement delattr(self, name).qhh]qi}qj(h}qk(hhhhu}ql(hh!hh"u�qmhX   Implement delattr(self, name).qnuauhhduX   __ne__qo}qp(h}qq(hX   Return self!=value.qrh]qs}qt(h}qu(hhhhu}qv(hh!hh"u�qwhX   Return self!=value.qxuauhhduX
   __reduce__qy}qz(h}q{(hX   helper for pickleq|h]q}}q~(h}q(hhhhu}q�(hh!hh"u�q�hX   helper for pickleq�uauhhduX   get_codeq�}q�(h}q�(hX9   Return None as built-in modules do not have code objects.q�h]q�}q�(h}q�(hhhhu}q�(hh!hh"u�q�hX9   Return None as built-in modules do not have code objects.q�uauhh%uX   load_moduleq�}q�(h}q�(hX�   Load the specified module into sys.modules and return it.

    This method is deprecated.  Use loader.exec_module instead.

    q�h]q�}q�(h}q�(hhhhu}q�(hh!hh"u�q�hX�   Load the specified module into sys.modules and return it.

    This method is deprecated.  Use loader.exec_module instead.

    q�uauhh%uX
   __format__q�}q�(h}q�(hX   default object formatterq�h]q�}q�(h}q�(hhhhu}q�(hh!hh"u�q�hX   default object formatterq�uauhhduX   __lt__q�}q�(h}q�(hX   Return self<value.q�h]q�}q�(h}q�(hhhhu}q�(hh!hh"u�q�hX   Return self<value.q�uauhhduX   __hash__q�}q�(h}q�(hX   Return hash(self).q�h]q�}q�(h}q�(hhhhu}q�(hh!hh"u�q�hX   Return hash(self).q�uauhhduX   __le__q�}q�(h}q�(hX   Return self<=value.q�h]q�}q�(h}q�(hhhhu}q�(hh!hh"u�q�hX   Return self<=value.q�uauhhduX   __repr__q�}q�(h}q�(hX   Return repr(self).q�h]q�}q�(h}q�(hhhhu}q�(hh!hh"u�q�hX   Return repr(self).q�uauhhduX   exec_moduleq�}q�(h}q�(hX   Exec a built-in moduleq�h]q�}q�(h}q�(hhhhu}q�(hh!hh"u�q�hX   Exec a built-in moduleq�uauhh%uX	   find_specq�}q�(h}q�h	]q�hX   methodq׆q�ashhuX
   __sizeof__q�}q�(h}q�(hX6   __sizeof__() -> int
size of object in memory, in bytesq�h]q�}q�(h}q�(h	]q�hX   objectq�q�ahX   selfq�u�q�hX"   size of object in memory, in bytesq�h7]q�h:auauhhduX
   get_sourceq�}q�(h}q�(hX8   Return None as built-in modules do not have source code.q�h]q�}q�(h}q�(hhhhu}q�(hh!hh"u�q�hX8   Return None as built-in modules do not have source code.q�uauhh%uh}q�(h}q�h	]q�hashhuX   __new__q�}q�(h}q�(hXG   Create and return a new object.  See help(type) for accurate signature.q�h]q�}q�(h}q�(hhhhu}q�(hh!hh"u�q�hXG   Create and return a new object.  See help(type) for accurate signature.q�uauhh%uX   find_moduleq�}q�(h}r   (hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        r  h]r  }r  (h}r  (hhhhu}r  (hh!hh"u�r  hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        r  uauhh%uX   __weakref__r  }r	  (h}r
  (hX2   list of weak references to the object (if defined)r  h	]r  hXauhX   propertyr  uX   __init__r  }r  (h}r  (hX>   Initialize self.  See help(type(self)) for accurate signature.r  h]r  }r  (h}r  (hhhhu}r  (hh!hh"u�r  hX>   Initialize self.  See help(type(self)) for accurate signature.r  uauhhduX   __dict__r  }r  (h}r  h	]r  hX   mappingproxyr  �r  ashhuX   __ge__r  }r  (h}r   (hX   Return self>=value.r!  h]r"  }r#  (h}r$  (hhhhu}r%  (hh!hh"u�r&  hX   Return self>=value.r'  uauhhduX   __eq__r(  }r)  (h}r*  (hX   Return self==value.r+  h]r,  }r-  (h}r.  (hhhhu}r/  (hh!hh"u�r0  hX   Return self==value.r1  uauhhduX   create_moduler2  }r3  (h}r4  (hX   Create a built-in moduler5  h]r6  }r7  (h}r8  (hhhhu}r9  (hh!hh"u�r:  hX   Create a built-in moduler;  uauhh%uX   __str__r<  }r=  (h}r>  (hX   Return str(self).r?  h]r@  }rA  (h}rB  (hhhhu}rC  (hh!hh"u�rD  hX   Return str(self).rE  uauhhduX   module_reprrF  }rG  (h}rH  (hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        rI  h]rJ  }rK  (h}rL  (hhhhu}rM  (hh!hh"u�rN  hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        rO  uauhh%uX   __subclasshook__rP  }rQ  (h}rR  (hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
rS  h]rT  }rU  (h}rV  (hhhhu}rW  (hh!hh"u�rX  hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
rY  uauhh%uX
   is_packagerZ  }r[  (h}r\  (hX4   Return False as built-in modules are never packages.r]  h]r^  }r_  (h}r`  (hhhhu}ra  (hh!hh"u�rb  hX4   Return False as built-in modules are never packages.rc  uauhh%uX   __dir__rd  }re  (h}rf  (hX.   __dir__() -> list
default dir() implementationrg  h]rh  }ri  (h}rj  (h	]rk  h�ahh�u�rl  hX   default dir() implementationrm  h7]rn  hX   listro  �rp  auauhhduX   __reduce_ex__rq  }rr  (h}rs  (hX   helper for picklert  h]ru  }rv  (h}rw  (hhhhu}rx  (hh!hh"u�ry  hX   helper for picklerz  uauhhduX
   __module__r{  }r|  (h}r}  h	]r~  hashhuX   __gt__r  }r�  (h}r�  (hX   Return self>value.r�  h]r�  }r�  (h}r�  (hhhhu}r�  (hh!hh"u�r�  hX   Return self>value.r�  uauhhduX	   __class__r�  }r�  (h]r�  hX   typer�  �r�  ahh,uuhX�   Meta path import for built-in modules.

    All methods are either class or static methods to avoid the need to
    instantiate the class.

    r�  X   mror�  ]r�  (h+hXeX	   is_hiddenr�  �uhh	uX   SIG_IGNr�  }r�  (h}r�  h	]r�  hJashhuX   SIGABRTr�  }r�  (h}r�  h	]r�  hJashhuX   SIGSEGVr�  }r�  (h}r�  h	]r�  hJashhuX   NSIGr�  }r�  (h}r�  h	]r�  hJashhuX   CTRL_C_EVENTr�  }r�  (h}r�  h	]r�  hJashhuX   SIG_DFLr�  }r�  (h}r�  h	]r�  hJashhuX   default_int_handlerr�  }r�  (h}r�  (hXj   default_int_handler(...)

The default handler for SIGINT installed by Python.
It raises KeyboardInterrupt.r�  h]r�  }r�  (h}r�  (hhhhu�r�  hXP   The default handler for SIGINT installed by Python.
It raises KeyboardInterrupt.r�  uauhh%uX   __package__r�  }r�  (h}r�  h	]r�  hashhuX   CTRL_BREAK_EVENTr�  }r�  (h}r�  h	]r�  hJashhuX   __spec__r�  }r�  (h}r�  h	]r�  h)X
   ModuleSpecr�  �r�  ashhuX   SIGBREAKr�  }r�  (h}r�  h	]r�  hJashhuX   SIGILLr�  }r�  (h}r�  h	]r�  hJashhuuu.